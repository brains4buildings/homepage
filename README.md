# Brains4Buildings: Public Code Repository
Welcome to the public repository for the Brains4Buildings (B4B) project. This repository is dedicated to gathering and sharing the algorithms, code, and software tools developed within the various work packages of the B4B consortium. Our goal is to foster transparency and enable public access to these tools, promoting that advancements made are available to the wider community.

Below, you'll find descriptions of each work package (WP) and links to their respective project groups.

## WP1: Smart Sensing and Data Acquisition
Work Package 1 focuses on developing smart diagnostic systems aimed at reducing energy losses in buildings through automated analysis. This system will enable performance-based maintenance planning by utilizing data from various building systems, such as heating, cooling, and lighting. The goal is to leverage sensor data and machine learning techniques to identify inefficiencies and provide actionable insights for real-time energy optimization.

Research topics:
- [4S3F fault detection](https://gitlab.com/brains4buildings/4s3f-fault-detection-wp1)
- [4S3F occupant comfort](https://gitlab.com/brains4buildings/4s3f-occupant-comfort-wp1)
- [FDD AHU](https://gitlab.com/brains4buildings/fdd-ahu-wp1)

## WP2: Data-Driven Modelling and Predition
Work Package 2 centers on the development of intelligent control models that increase energy flexibility within buildings. The primary aim is to optimize energy consumption by balancing heat, cold, and electricity usage, while minimizing CO2 emissions. These models will account for user comfort, local energy resources, and cost efficiency. This work includes the integration of predictive models that forecast energy demand and supply in real-time.

Research topics:
- [MPC energy flexibility](https://gitlab.com/brains4buildings/mpc-energy-flexibility-wp2)
- [Prediction models](https://gitlab.com/brains4buildings/prediction-models-wp2)

## WP3: Control Strategies for Energy and Comfort Optimization
Work Package 3 develops user interfaces for both end users and facility managers to ensure energy-efficient and healthy indoor environments. This package also aims to encourage energy-saving behaviors and flexibility through intelligent control interfaces. Interfaces will focus on providing real-time feedback to users to help them make informed decisions about energy consumption without compromising comfort.

Research topics:
- [Feedback interfaces](https://gitlab.com/brains4buildings/feedback-interfaces-wp3)
- [User confort and feedback](https://gitlab.com/brains4buildings/user-confort-and-feedback-wp3)

## WP4: Building User Interaction and Feedback
Work Package 4 addresses data connectivity between different applications, ensuring that data is transmitted securely, ethically, and in a standardized manner. It involves researching system integration at the API level and managing data privacy and security across various devices and platforms. The aim is to create a seamless exchange of information that contributes to building intelligence and energy efficiency.

Research topics:
- [Generate meta data scheme](https://gitlab.com/brains4buildings/generate-meta-data-scheme-wp4)
- [Live data access 4 control](https://gitlab.com/brains4buildings/live-data-access-4-control-wp4)
- [Smart readiness assessment](https://gitlab.com/brains4buildings/smart-readiness-assessment-wp4)

## WP5: Demonstration and Testing in Real Buildings
Work Package 5 focuses on real-world demonstrations and testing of the developed prototypes in living labs and use cases. This package evaluates the market value of the proposed solutions, validating them in actual buildings. The outcomes will serve as best practices for further scaling and market introduction.